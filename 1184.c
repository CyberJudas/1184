#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>

Display *dpy;
int screen;

struct Root {
	Window win;
	int x, y;
	unsigned int width, height;
	unsigned int layout;
} root;


void start();
void grab();
void loop();
void stop();
void die();

void maprequest(XEvent *event);

void (*handle_events[LASTEvent])(XEvent *event) = {
    [MapRequest] = maprequest,
    };

int main() {
	start();
	grab();
	loop();
	stop();
	return 0;
}

void die(char *exit_msg) {
         fprintf(stderr, "%s\n", exit_msg); /* Debug message before exit */
         exit(EXIT_FAILURE);
}

void start() {
	if (!(dpy = XOpenDisplay(0)))
		die("failed to open display");
	
	screen = DefaultScreen(dpy);

	root.win = XDefaultRootWindow(dpy);
	root.width = XDisplayWidth(dpy, screen);
	root.height = XDisplayHeight(dpy, screen);

	XSelectInput(dpy, root.win, SubstructureRedirectMask);

}

void loop() {
	XEvent ev;

	while (!XNextEvent(dpy, &ev)) {
		
		if (handle_events[ev.type]) {
			handle_events[ev.type](&ev);
			}
		}	
}

void grab() {

}

void stop() {
	XCloseDisplay(dpy);
}

void maprequest(XEvent *event) {
	XMapRequestEvent *ev = &event -> xmaprequest;
	XMoveResizeWindow(dpy, ev -> window, 0, 0, root.width, root.height);
	XMapWindow(dpy, ev -> window);

}
