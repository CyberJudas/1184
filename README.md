1184 - A basic window manager
========
1184 is an extremely basic window manager for X, written in C.

This currently consists of about 80 lines of code, and at the moment, lacks MANY basic features of a window manager. 

It simply binds every window to the top left of the screen, and makes it cover the entire screen, Making this a fullscreen stacking window manager.

Requirements
--------
A C compiler, (Like gcc)

Xlib header files,

Xinit,

A keyboard shortcut manager, (Like sxhkd or xbindkeys)

A terminal emulator, (Anything works, I would recommend st.)

An application launcher (Like dmenu or rofi)

Installation
--------
Clone this repository.

The source code needs to be compiled in order for the window manager to run. (duh)
    
    gcc 1184.c -o 1184 -lX11

(Be sure to add the -lX11 tag to link the X11 headers!)

After it's compiled, move the executable to /usr/local/bin, so that it can be executed with the command '1184'
   
    mv 1184 /usr/local/bin

Be sure to configure your shortcut manager to at least spawn a terminal and a application launcher, and make sure it's active before the window manager is executed, Or else 1184 will be more or less unusable.

Then add the following to the end of your .xinitrc:

    exec 1184

After running startx, the window manager should work, Don't expect much from it though.

Configuration
--------
To configure and customize 1184, delete the existing 1184 executable and edit the 1184.c file however you want, then recompile it.

1184 is still Work-in-Progress. Expect more features in the future.